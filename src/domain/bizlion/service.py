from src.domain.bizlion.entities import (
    BindingMemberReqSchema,
    ExpHistoryRespSchema,
    OverviewRespSchema,
)
from src.utils import aware_now_timestamp  # WrappedException


class BizlionService:

    _binding_member_req_schema = BindingMemberReqSchema()
    _overiew_resp_schema = OverviewRespSchema()
    _exp_history_resp_schema = ExpHistoryRespSchema()

    def __init__(self, repository, bizlion_client):
        self.repo = repository
        self.bizlion_client = bizlion_client

    """
    1. 綁定
      - post
        binding_member
    2. 取得會員總攬與條碼 (還有網站連結)
      - get
        get_member_overview
    3. 取得積分歷程
      - get
        get_member_exp_history
    """

    def binding_member(self, binding_req):
        """binding_req = { 'member_hash': member_hash, 'phone': phone}"""
        binding_req = self._binding_member_req_schema.load(binding_req)
        self.repo.add_member(binding_req)
        return self.bizlion_client.binding_member(binding_req)

    def get_member_overview(self, member_hash):
        """get member's info and qrcode text."""
        user_token = self.get_token(member_hash)
        member_info = self.bizlion_client.get_member_info(user_token)
        member_qrcode = self.bizlion_client.get_member_qrcode(user_token)
        member_links = self.bizlion_client.get_member_links(user_token)
        return self._overiew_resp_schema.dump(
            {**member_info, **member_qrcode, **member_links}
        )

    def get_member_exp_history(self, member_hash):
        user_token = self.get_token(member_hash)
        exp_history = self.bizlion_client.get_member_exp_history(user_token)
        return self._exp_history_resp_schema.dump(exp_history)

    def get_token(self, member_hash):
        """if token was expired then refresh token by api, else get token from db"""
        member = self.repo.get_member(member_hash)
        token_expire_time = member["expire_time"]
        if token_expire_time < aware_now_timestamp():
            phone = member["phone"]
            user_token = self.bizlion_client.get_user_token(phone)
            self.repo.set_user_token(member_hash, user_token)
        else:
            user_token = self.repo.get_user_token(member_hash)
        return user_token
