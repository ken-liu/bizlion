import abc
import enum

from src.utils import WrappedException


class ErrorMessage(enum.Enum):
    EXISTS_PHONE_NUMBER = "綁定失敗，此手機號碼已被綁定，一隻手機僅能綁定一隻裝置，有任何問題請聯繫客服。"
    EXISTS_MEMBER_HASH = "綁定失敗，此會員帳號已註冊，一個會員帳號僅能註冊一個商業獅會員，有任何問題請聯繫客服。"
    NOT_MEMBER = "尚未綁定會員，請先完成綁定"


class BizlionRepositoryException(WrappedException):
    pass


class AbstractBizlionRepository(abc.ABC):
    @abc.abstractmethod
    def add_member(self, binding_req):
        pass

    @abc.abstractmethod
    def get_user_token(self, member_hash):
        pass

    @abc.abstractmethod
    def get_member(self, member_hash):
        pass

    @abc.abstractmethod
    def set_user_token(self, member_hash, user_token):
        pass
