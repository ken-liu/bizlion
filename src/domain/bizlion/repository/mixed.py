from datetime import timedelta

from sqlalchemy import Column, String
from sqlalchemy.sql import exists

from config import MYSQL_URI
from src.domain.bizlion.repository.base import (
    AbstractBizlionRepository,
    BizlionRepositoryException,
    ErrorMessage,
)
from src.infra.mysql import Base, Integer, UnsignedInt, get_session
from src.utils import aware_now, aware_now_timestamp


class MemberModel(Base):
    __tablename__ = "member"

    idx = Column(UnsignedInt, primary_key=True, autoincrement=True)
    member_hash = Column(String(50), nullable=False, index=True, unique=True)
    phone = Column(String(10), nullable=False, index=True, unique=True)
    user_token = Column(String(100))
    expire_time = Column(Integer, default=aware_now_timestamp, index=True)


class ErrorLogModel(Base):
    __tablename__ = "error_log"

    idx = Column(UnsignedInt, primary_key=True, autoincrement=True)
    time = Column(Integer, default=aware_now_timestamp, index=True)
    member_hash = Column(String(50), nullable=False, index=True)
    phone = Column(String(10), nullable=False, index=True)
    description = Column(String(50))


class MixedBizlionRepository(AbstractBizlionRepository):
    def __init__(self, mysql_uri=MYSQL_URI):
        self.mysql_uri = mysql_uri

    def get_user_token(self, member_hash):
        """if token expired then get a new token, else return token."""
        with get_session(self.mysql_uri) as session:
            obj = session.query(MemberModel).filter_by(member_hash=member_hash).first()
            if not obj:
                raise BizlionRepositoryException(
                    ErrorMessage.NOT_MEMBER.value, code=403
                )
            member = obj.dump()
            return member["user_token"]

    def add_member(self, binding_req):
        """
        add new member to db.
            - if phone or member_hash have been binding, add error log.
        """
        error_message = ""
        with get_session(self.mysql_uri) as session:
            if session.query(
                exists().where(MemberModel.phone == binding_req["phone"])
            ).scalar():
                error_log = {**binding_req, "description": "手機號碼已註冊"}
                session.merge(ErrorLogModel(**error_log))
                error_message = ErrorMessage.EXISTS_PHONE_NUMBER.value
            elif session.query(
                exists().where(MemberModel.member_hash == binding_req["member_hash"])
            ).scalar():
                error_log = {**binding_req, "description": "裝置已綁定過商業獅會員"}
                session.merge(ErrorLogModel(**error_log))
                error_message = ErrorMessage.EXISTS_PHONE_NUMBER.value
            else:
                session.merge(MemberModel(**binding_req))

        # raise error message
        if error_message:
            raise BizlionRepositoryException(
                error_message, code=403,
            )

    def get_member(self, member_hash):
        with get_session(self.mysql_uri) as session:
            obj = session.query(MemberModel).filter_by(member_hash=member_hash).first()
            if not obj:
                raise BizlionRepositoryException(
                    ErrorMessage.NOT_MEMBER.value, code=403
                )
            member = obj.dump()
            return member

    def set_user_token(self, member_hash, user_token):
        """set new token to db"""
        with get_session(self.mysql_uri) as session:
            query = session.query(MemberModel).filter_by(member_hash=member_hash)
            expire_time = (aware_now() + timedelta(days=1)).timestamp()
            query.update({"user_token": user_token, "expire_time": expire_time})
