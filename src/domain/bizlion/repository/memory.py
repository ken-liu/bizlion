import random
import string
from datetime import timedelta

from src.domain.bizlion.repository.base import (
    AbstractBizlionRepository,
    BizlionRepositoryException,
    ErrorMessage,
)
from src.utils import aware_now, aware_now_timestamp


class InMemoryBizlionRespository(AbstractBizlionRepository):
    def __init__(self):
        self.members = {}

    def get_user_token(self, member_hash):
        if member_hash in self.members.keys():
            if self.members[member_hash]["expire_time"] < aware_now_timestamp():
                self.refresh_user_token(self.members[member_hash])
            return self.members[member_hash]["user_token"]
        else:
            raise BizlionRepositoryException(
                f"member_hash {member_hash} not found", code=404
            )

    def add_member(self, binding_req):
        if binding_req["phone"] in [self.members[m]["phone"] for m in self.members]:
            raise BizlionRepositoryException(
                ErrorMessage.EXISTS_PHONE_NUMBER.value, code=403,
            )
        elif binding_req["member_hash"] in self.members:
            raise BizlionRepositoryException(
                ErrorMessage.EXISTS_MEMBER_HASH.value, code=403,
            )
        else:
            self.generate_member(**binding_req)

    def get_member(self, member_hash):
        if member_hash in self.members:
            return self.members[member_hash]
        raise BizlionRepositoryException(ErrorMessage.NOT_MEMBER.value, code=403)

    def set_user_token(self, member_hash, user_token):
        self.members[member_hash]["user_token"] = user_token
        self.members[member_hash]["expire_time"] = (
            aware_now() + timedelta(days=1)
        ).timestamp()

    def generate_member(self, member_hash, phone):
        user_token, expire_time = self.generate_token()
        self.members.update(
            {
                member_hash: {
                    "idx": (
                        max([self.members[m]["idx"] for m in self.members])
                        if self.members
                        else 0
                    )
                    + 1,
                    "member_hash": member_hash,
                    "phone": phone,
                    "user_token": user_token,
                    "expire_time": expire_time,
                }
            }
        )
        print(self.members[member_hash])

    def generate_token(self):
        return (
            "".join(random.choices(string.ascii_letters + string.digits + "_", k=86)),
            (aware_now() + timedelta(days=1)).timestamp(),
        )

    def refresh_user_token(self, member_info):
        member_info["user_token"], member_info["expire_time"] = self.generate_token()
