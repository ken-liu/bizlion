from marshmallow import fields, validate

from src.utils import BaseSchema


class BindingMemberReqSchema(BaseSchema):
    member_hash = fields.String(required=True)
    phone = fields.String(required=True, validate=validate.Regexp(r"^09\d{8}$"))


class OverviewRespSchema(BaseSchema):
    coupon_num = fields.Int(required=True, description="優惠券總數")
    card_num = fields.Int(required=True, description="集點卡總數")
    exp_num = fields.Int(required=True, description="積分總數")
    qr_code = fields.String(required=True, description="會員條碼")
    expire_time = fields.Int(required=True, description="條碼過期時間(timestamp)")
    coupon_link = fields.String(required=True, description="「優惠券」連結")
    card_link = fields.String(required=True, description="「集點卡」連結")
    store_link = fields.String(required=True, description="「找商店」連結")
    contact_link = fields.String(required=True, description="「聯絡我們」連結")


class ExpChangeSchema(BaseSchema):
    oper_date = fields.Int(required=True, description="積分變化日期(timestamp)")
    exp_change = fields.Int(required=True, description="積分變化")
    memo = fields.String(required=True, description="簡述")


class ExpHistoryRespSchema(BaseSchema):
    exp_changes = fields.List(fields.Nested(ExpChangeSchema))
