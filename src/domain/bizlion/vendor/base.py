import abc
import enum

from config import LION_OS_BASE_URL
from src.utils import WrappedException

# from datetime import datetime


class LinkEnum(enum.Enum):
    COUPON_LINK = f"{LION_OS_BASE_URL}/Coupon/List"
    CARD_LINK = f"{LION_OS_BASE_URL}/MyPoint"
    STORE_LINK = f"{LION_OS_BASE_URL}/Store/List"
    CONTACT_LINK = f"{LION_OS_BASE_URL}/Home/Contact"


class BizlionClientException(WrappedException):
    pass


class AbstractBizlionClinet(abc.ABC):
    @abc.abstractmethod
    def get_user_token(self, phone):
        pass

    @abc.abstractmethod
    def binding_member(self, binding_req):
        pass

    @abc.abstractmethod
    def get_member_info(self, user_token):
        pass

    @abc.abstractmethod
    def get_member_qrcode(self, user_token):
        pass

    @abc.abstractmethod
    def get_member_exp_history(self, user_token):
        pass

    def get_member_links(self, user_token):
        return {
            "coupon_link": f"{LinkEnum.COUPON_LINK.value}?usertoken={user_token}",
            "card_link": f"{LinkEnum.CARD_LINK.value}?usertoken={user_token}",
            "store_link": f"{LinkEnum.STORE_LINK.value}?usertoken={user_token}",
            "contact_link": f"{LinkEnum.CONTACT_LINK.value}?usertoken={user_token}",
        }


class FakeBizlionClient(AbstractBizlionClinet):
    def __init__(self):
        self.members = {}

    def get_user_token(self, phone):
        return (
            "vaUUhfxoLl21nsI-kGhQVFun_"
            "JaVvQ0Bi7cdOe84RnBQbd06JzJ0ywPl48eI3XaBakMeOBTrQFpgiY3Vtbn2Ng"
        )

    def binding_member(self, binding_req):
        self.members[binding_req["member_hash"]] = dict(binding_req)
        return {"message": "綁定成功"}

    def get_member_info(self, user_token):
        return {
            "qr_code": "29743-f9ERd3A5",
            "expire_time": "1591071503",
        }

    def get_member_qrcode(self, user_token):
        return {
            "coupon_num": "5",
            "card_num": "0",
            "exp_num": "10",
        }

    def get_member_exp_history(self, user_token):
        return {
            "exp_changes": [
                {"oper_date": "1591070103", "exp_change": "7", "memo": "活動累積點數"},
                {"oper_date": "1591068103", "exp_change": "3", "memo": "活動累積點數"},
            ]
        }
