import ast
import os
import pathlib

import requests

from config import BIZ_PASSWORD, BIZ_USER_NAME, LION_API_BASE_URL, X_WWW_FORM_URLENCODED
from src.domain.bizlion.vendor.base import AbstractBizlionClinet, BizlionClientException
from src.utils import aware_now_timestamp


class BizlionClient(AbstractBizlionClinet):
    base_url = LION_API_BASE_URL
    token_path = str((pathlib.Path(__file__).parent / ".access_token").absolute())

    def __init__(self):
        if os.path.exists(self.token_path):
            with open(self.token_path, "r") as f:
                access_json = ast.literal_eval(f.read())
                self.token = access_json["access_token"]
                self.token_expired = access_json[".expires"] / 1000
                self.refresh_token = access_json["refresh_token"]
        else:
            self.refresh_access_token()

    def refresh_access_token(self):
        if getattr(self, "refresh_token", 0):
            res = requests.post(
                f"{self.base_url}/Token",
                headers={"Content-Type": X_WWW_FORM_URLENCODED},
                data=f"grant_type=refresh_token&refresh_token={self.refresh_token}",
            )
        else:
            res = requests.post(
                f"{self.base_url}/Token",
                headers={"Content-Type": X_WWW_FORM_URLENCODED},
                data=(
                    f"grant_type=password&"
                    f"username={BIZ_USER_NAME}&"
                    f"password={BIZ_PASSWORD}"
                ),
            )
        res_json = res.json()
        self.token = res_json["access_token"]
        self.token_expired = res_json[".expires"] / 1000
        self.refresh_token = res_json["refresh_token"]
        with open(self.token_path, "w") as f:
            f.write(f"{res_json}")

    def get_headers(self):
        # refresh access_token in headers if expired
        now = aware_now_timestamp()
        if now >= self.token_expired:
            self.refresh_access_token()
        return {"Authorization": f"bearer {self.token}"}

    def wrapped_request(self, method, url, payload=None):
        def _wrapped_request():
            headers = self.get_headers()
            if method == "GET":
                res = requests.get(url, headers=headers)
            elif method == "POST":
                res = requests.post(url, json=payload, headers=headers)
            else:
                raise NotImplementedError
            return res

        res = _wrapped_request()
        try:
            if "error_code" in res.json():
                self.refresh_access_token()
                res = _wrapped_request()
        finally:
            return res

    def get_user_token(self, phone):
        payload = {"PhoneNum": phone}
        resp = self.wrapped_request(
            "POST", f"{self.base_url}/BizLion/GetUserTokenByPhone", payload
        ).json()
        return resp["UserToken"]

    def binding_member(self, binding_req):
        payload = {"PhoneNum": binding_req["phone"]}
        self.wrapped_request(
            "POST", f"{self.base_url}/BizLion/BindingUser_Inovs", payload
        )
        print(
            f"綁定成功 \n"
            f"  phone:       {binding_req['phone']}\n"
            f"  member_hash: {binding_req['member_hash']}"
        )
        return {"message": "綁定成功"}

    def get_member_info(self, user_token):
        payload = {"UserToken": user_token}
        resp = self.wrapped_request(
            "POST", f"{self.base_url}/BizLion/GetUserOverview", payload
        ).json()
        if "Message" in resp:
            raise BizlionClientException(
                f"{resp['Message']} 因為{resp['ModelState']['UserToken'][0]}", code=502
            )
        return {
            "coupon_num": resp["CouponNum"],
            "card_num": resp["CardNum"],
            "exp_num": resp["ExpNum"],
        }

    def get_member_qrcode(self, user_token):
        payload = {"UserToken": user_token}
        resp = self.wrapped_request(
            "POST", f"{self.base_url}/BizLion/GetUserQRcode", payload
        ).json()
        return {"qr_code": resp["QRcode"], "expire_time": resp["ExpireTime"]}

    def get_member_exp_history(self, user_token):
        payload = {"UserToken": user_token}
        resp = self.wrapped_request(
            "POST", f"{self.base_url}/BizLion/GetUserExpHistory", payload
        ).json()
        if "Message" in resp:
            raise BizlionClientException(
                f"{resp['Message']} 因為{resp['ModelState']['UserToken'][0]}", code=502
            )
        return {
            "exp_changes": [
                (
                    {
                        "oper_date": r["OperDate"],
                        "exp_change": r["ExpChange"],
                        "memo": r["Memo"],
                    }
                )
                for r in resp
            ]
        }
