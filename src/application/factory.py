import sqlalchemy_utils

from config import DEV, ENV, MYSQL_URI, PRODUCTION, STAGING
from src.domain.bizlion import (
    BizlionClient,
    BizlionService,
    FakeBizlionClient,
    InMemoryBizlionRespository,
    MixedBizlionRepository,
)
from src.infra.mysql import Base, get_session

if ENV == DEV:
    biz_repository = InMemoryBizlionRespository()
    biz_client = FakeBizlionClient()
    bizlion_service = BizlionService(InMemoryBizlionRespository(), FakeBizlionClient())
elif ENV == STAGING:
    if not sqlalchemy_utils.database_exists(MYSQL_URI):
        sqlalchemy_utils.create_database(MYSQL_URI, encoding="utf8mb4")
    with get_session(MYSQL_URI) as session:
        Base.metadata.create_all(bind=session.bind)
    biz_repository = MixedBizlionRepository(MYSQL_URI)
    biz_client = FakeBizlionClient()
elif ENV == PRODUCTION:
    if not sqlalchemy_utils.database_exists(MYSQL_URI):
        sqlalchemy_utils.create_database(MYSQL_URI, encoding="utf8mb4")
    with get_session(MYSQL_URI) as session:
        Base.metadata.create_all(bind=session.bind)
    biz_repository = MixedBizlionRepository(MYSQL_URI)
    biz_client = BizlionClient()
else:
    raise NotImplementedError()

bizlion_service = BizlionService(biz_repository, biz_client)
