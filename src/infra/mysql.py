from contextlib import contextmanager

from sqlalchemy import Integer, create_engine
from sqlalchemy.dialects.mysql import INTEGER  # noqa
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker


def dump(self):
    return {c.name: getattr(self, c.name) for c in self.__table__.columns}


Base = declarative_base()
Base.dump = dump

UnsignedInt = Integer()
UnsignedInt = UnsignedInt.with_variant(INTEGER(unsigned=True), "mysql")
SESSION_MAP = {}


@contextmanager
def get_session(mysql_uri, reuse=True):

    if not (reuse and SESSION_MAP.get(mysql_uri)):
        engine = create_engine(mysql_uri, pool_pre_ping=True, pool_recycle=1800,)
        Session = scoped_session(
            sessionmaker(autocommit=False, autoflush=False, bind=engine)
        )
        if reuse:
            SESSION_MAP[mysql_uri] = Session
    else:
        Session = SESSION_MAP[mysql_uri]

    try:
        s = Session()
        yield s
        s.commit()
    except Exception as err:
        s.rollback()
        raise err
    finally:
        s.close()


def insert_or_update(model, data, filter_kwargs, session):
    query = session.query(model).filter_by(**filter_kwargs)
    data = {
        k: v for k, v in data.items() if k in [c.name for c in model.__table__.columns]
    }
    if query.first():
        query.update(data)
    else:
        session.merge(model(**data))
