import json
from datetime import datetime

import pytz
from marshmallow import Schema

TIMEZONE = pytz.timezone("Asia/Taipei")


def aware_now():
    return datetime.now(TIMEZONE)


def aware_now_timestamp():
    return datetime.now(TIMEZONE).timestamp()


def jdumps(d):
    return json.dumps(d, ensure_ascii=False, indent=2, default=str)


class WrappedException(Exception):
    def __init__(self, messages, code=500):
        self.messages = messages
        self.code = code


class BaseSchema(Schema):
    class Meta:
        ordered = True  # for swagger schema sorting
