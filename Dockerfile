FROM python:3.8-slim-buster

COPY ./requirements.txt /pyenv/
RUN pip install -r /pyenv/requirements.txt

RUN useradd --create-home appuser
USER appuser

RUN mkdir /home/appuser/invos-payment
COPY --chown=appuser:appuser ./ /home/appuser/invos-payment/
WORKDIR /home/appuser/invos-payment
CMD ["/bin/bash"]
