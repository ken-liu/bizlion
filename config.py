import os
import pathlib
import platform

from dotenv import load_dotenv

dotenv_path = pathlib.Path(__file__).resolve().parent / ".env"
load_dotenv(dotenv_path)

DEV = "dev"
STAGING = "staging"
PRODUCTION = "production"
ENV = os.environ["ENV"]
DEBUG = True if ENV == DEV else False

# localhost in OS X docker will be host.docker.internal
LOCALHOST = os.environ["LOCALHOST"]
localhost = "127.0.0.1" if platform.system() == "Darwin" else LOCALHOST

APP_PORT = os.environ["APP_PORT"]
APP_BASE_URL = os.environ["APP_BASE_URL"]

MYSQL_USER = os.environ["MYSQL_USER"]
MYSQL_PASSWORD = os.environ["MYSQL_PASSWORD"]
MYSQL_HOST = os.environ["MYSQL_HOST"].replace(LOCALHOST, localhost)
MYSQL_PORT = os.environ["MYSQL_PORT"]
MYSQL_DATABASE = os.environ["MYSQL_DATABASE"]
MYSQL_URI = (
    f"mysql+pymysql://{MYSQL_USER}:{MYSQL_PASSWORD}@{MYSQL_HOST}:{MYSQL_PORT}"
    f"/{MYSQL_DATABASE}?charset=utf8mb4"
)

LION_API_BASE_URL = os.environ["LION_API_BASE_URL"]
# "https://biz.ipickup.com.tw/api"
LION_OS_BASE_URL = os.environ["LION_OS_BASE_URL"]
# "https://www.bizlion.com.tw/bizlion"
X_WWW_FORM_URLENCODED = os.environ["X_WWW_FORM_URLENCODED"]
# "application/x-www-form-urlencoded;charset=utf-8"

BIZ_USER_NAME = os.environ["BIZ_USER_NAME"]
BIZ_PASSWORD = os.environ["BIZ_PASSWORD"]
