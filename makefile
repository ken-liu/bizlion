include .env

# include .env

PYTHON=.venv/bin/python
PYTEST=.venv/bin/pytest
GUNICORN=.venv/bin/gunicorn
ANSIBLE_PLAYBOOK=.venv/bin/ansible-playbook
PRE_COMMIT=.venv/bin/pre-commit

.PHONY: run run-staging run-production
all: test pre

.venv/bin/activate: requirements.txt
	test -f .venv/bin/activate || rm -rf .venv/ && virtualenv --python=$(shell (which python3.8 || which python3)) .venv
	. .venv/bin/activate && pip install -r requirements-dev.txt -r requirements.txt && pre-commit install
test: .venv/bin/activate
	ENV=dev $(PYTEST) -k "not vendor"
pre:
	$(PRE_COMMIT) run --all-files
clean:
	find . -name '*.pyc' -delete
	find . -name '*.pyo' -delete
	find . -name '*~' -delete
	find . -name '__pycache__' -delete
	find . -name 'geckodriver.log' -delete

# for dev VPN: flush current ip when wifi changed
flush-osx-wifi:
	sudo bash -c "time (ifconfig en0 down && route flush && ifconfig en0 up)"

run: .venv/bin/activate
	$(PYTHON) -m src.application.rest
build:
	docker-compose -f service.yml build
logs:
	docker-compose -f service.yml logs --tail=20 -f api scheduler worker

purge-ssh-tunnels:
	ps aux | grep -E 'autossh|ssh.*inpayp' | grep -v grep | awk '{print $$2}' | xargs kill -9
up-ssh-tunnels: purge-ssh-tunnels
	autossh -f -M 0 -N -o "ServerAliveInterval 10" -o "ServerAliveCountMax 3" -o ExitOnForwardFailure=yes -D 3128 inpayp
	autossh -f -M 0 -N -o "ServerAliveInterval 10" -o "ServerAliveCountMax 3" -o ExitOnForwardFailure=yes -R ${NGINX_PORT}:localhost:${APP_PORT} inpayp

up-staging: up-ssh-tunnels
	docker-compose -f staging-db.yml up -d
test-staging: down-staging up-staging
	ENV=staging $(PYTEST) -k "not vendor"
down-staging:
	docker-compose -f staging-db.yml -f service.yml down && make purge-ssh-tunnels
run-staging: up-staging
	docker-compose -f service.yml up -d
rebuild-staging:
	make build && make up-ssh-tunnels && docker-compose -f staging-db.yml -f service.yml up -d && docker system prune -f

down-production:
	docker-compose -f service.yml down && make purge-ssh-tunnels
run-production: up-ssh-tunnels
	docker-compose -f service.yml up -d
rebuild-production:
	make build && make up-ssh-tunnels && docker-compose -f service.yml up -d && docker system prune -f

deploy-nginx:
	$(ANSIBLE_PLAYBOOK) settings/playbooks/setup-nginx.yml
deploy-staging:
	$(ANSIBLE_PLAYBOOK) settings/playbooks/common.yml -l staging
	$(ANSIBLE_PLAYBOOK) settings/playbooks/install-docker.yml -l staging
	. .env.staging && PROXY_HOST=$$PROXY_HOST $(ANSIBLE_PLAYBOOK) settings/playbooks/setup-staging.yml
	.venv/bin/ansible staging -m shell -a 'cd ~/invos-payment && make rebuild-staging'
deploy-production:
	$(ANSIBLE_PLAYBOOK) settings/playbooks/common.yml -l production
	$(ANSIBLE_PLAYBOOK) settings/playbooks/install-docker.yml -l production
	. .env.production && PROXY_HOST=$$PROXY_HOST $(ANSIBLE_PLAYBOOK) settings/playbooks/setup-production.yml
	.venv/bin/ansible production -m shell -a 'cd ~/invos-payment && make rebuild-production'
